//
//  ProfileViewController.swift
//  FitRx
//
//  Created by Pragya  Lamsal on 4/12/20.
//  Copyright © 2020 Ami  Lamsal. All rights reserved.
//

import UIKit
import CoreData

class ProfileViewController: UIViewController {
    
        
        @IBOutlet weak var TableView: UITableView!
        var people = [Person]() // changes string to Person
      override func viewDidLoad() {
        super.viewDidLoad()
         let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
               
               do {
                   let people = try PersistenceServce.context.fetch(fetchRequest)
                   self.people = people
                   self.TableView.reloadData()

               } catch {}

        }
        
        @IBAction func onPlusTapped() {
            let alert = UIAlertController(title: "Add Person", message: nil, preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder = "Name"
                }
                alert.addTextField { (textField) in
                    textField.placeholder = "Age"
                    textField.keyboardType = .numberPad
            }
            alert.addTextField { (textField) in
                         textField.placeholder = "Height (in)"
                         textField.keyboardType = .numberPad
                 }
            alert.addTextField { (textField) in
                         textField.placeholder = "Weight (kg)"
                         textField.keyboardType = .numberPad
                 }
                
             let action = UIAlertAction(title: "Enter", style: .default) { (_) in
                let name = alert.textFields!.first!.text!
                let age = alert.textFields!.first!.text!
                let height = alert.textFields!.first!.text!
                let weight = alert.textFields!.first!.text!
                let person = Person(context: PersistenceServce.context) // add these in
                person.name = name
                person.age = Int16 (age)!
                person.height = Int16 (height)!
                person.weight = Int16 (weight)!
                PersistenceServce.saveContext()
                self.people.append(person) // then this
                self.TableView.reloadData()
            }
            alert.addAction(action)
            present(alert,animated: true, completion: nil)
            
        }
    }

    extension ProfileViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return people.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
            cell.textLabel?.text = people [indexPath.row].name //add these
            cell.detailTextLabel?.text = String(people[indexPath.row].age)
            cell.detailTextLabel?.text = String(people[indexPath.row].height)
            cell.detailTextLabel?.text = String(people[indexPath.row].weight)
            return cell
        }
    }
