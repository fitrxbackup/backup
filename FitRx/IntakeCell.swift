//
//  IntakeCell.swift
//  FitRx
//
//  Created by Pragya  Lamsal on 5/1/20.
//  Copyright © 2020 Ami  Lamsal. All rights reserved.
//

import UIKit

class IntakeCell: UITableViewCell {
    
    
    @IBOutlet var todoLabel: UILabel!
    
    @IBOutlet var fitrxImage: UIImageView!
    
    var isChecked = false

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
