//
//  Person+CoreDataProperties.swift
//  FitRx
//
//  Created by studentadmin on 5/1/20.
//  Copyright © 2020 Ami  Lamsal. All rights reserved.
//
//

import Foundation
import CoreData

extension Person {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
}

    @NSManaged public var age: Int16
    @NSManaged public var height: Int16
    @NSManaged public var name: String
    @NSManaged public var weight: Int16

}
