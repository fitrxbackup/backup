//
//  Person+CoreDataClass.swift
//  FitRx
//
//  Created by studentadmin on 5/1/20.
//  Copyright © 2020 Ami  Lamsal. All rights reserved.
//
//

import Foundation
import CoreData
@objc(Person)
public class Person: NSManagedObject {}
