//
//  IntakeViewController.swift
//  FitRx
//
//  Created by Pragya  Lamsal on 4/12/20.
//  Copyright © 2020 Ami  Lamsal. All rights reserved.
//

import UIKit



class IntakeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var todoTableView: UITableView!
    
    var todos: [String] = ["30 ounces of water", "Salad", "One apple"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        todoTableView.delegate = self
        todoTableView.dataSource = self
            }
    
    
    
  
    @IBAction func addTodo(_ sender: Any) {
        let todoAlert = UIAlertController (title: "Add", message: "Add item", preferredStyle: .alert)
        todoAlert.addTextField()
        let addTodoAction = UIAlertAction (title: "Add", style: .default) { (action) in
            let newTodo = todoAlert.textFields![0]
            self.todos.append(newTodo.text!)
            self.todoTableView.reloadData()
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        
        todoAlert.addAction(addTodoAction)
        todoAlert.addAction(cancelAction)
        
        present(todoAlert, animated: true, completion: nil)
        
    }
    
    



    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "todoCell", for: indexPath) as! IntakeCell
        
        cell.todoLabel.text = todos[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! IntakeCell
        
        if cell.isChecked == false {
            cell.fitrxImage.image = UIImage(named: "circle-cropped.png")
            cell.isChecked = true
        }
        else {
            cell.fitrxImage.image = nil
            cell.isChecked = false
        }
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            todos.remove(at: indexPath.row)
            todoTableView.reloadData()
        }
    }

}
